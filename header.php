<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package hacienda_teya
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-71811662-1', 'auto');
    ga('send', 'pageview');

</script>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<header id="masthead" class="site-header" role="banner">
        <div class="barra_menu">
            <div class="nav">
                <img src="<?php echo get_template_directory_uri() ?>/img/navigation.png">
            </div>
            <div class="text-center logo_movil">
                <img src="<?php echo get_template_directory_uri() ?>/img/logo.png">
            </div>
        </div>
		<nav id="site-navigation" class="main-navigation" role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
		</nav>
        <nav id="site-navigation" class="main-navigation_movil" role="navigation">
            <a class="close_menu">X</a>
            <?php wp_nav_menu( array( 'theme_location' => 'movil', 'menu_id' => 'movil-menu' ) ); ?>
        </nav>
	</header>

    <div id="content" class="large-12 columns site-content">
