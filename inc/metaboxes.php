<?php
add_action('add_meta_boxes', 'cyb_meta_boxes');
function cyb_meta_boxes() {
    $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post'] ;
    $template_file = get_post_meta($post_id,'_wp_page_template',TRUE);
    if ($template_file == 'template-page/tpl-home.php') {
        add_meta_box('cyb-meta-restaurante', __('Restaurante'), 'cyb_meta_restaurante', 'page');
        add_meta_box('cyb-meta-historia', __('Historia de la hacienda'), 'cyb_meta_historia', 'page');
        add_meta_box('cyb-meta-organizaevento', __('Organiza tu evento'), 'cyb_meta_organizaevento', 'page');
    }
    if ($template_file == 'template-page/tpl-eventos.php') {
        add_meta_box('cyb-meta-hospedaje', __('Texto hospedaje'), 'cyb_meta_hospedaje', 'page');
        add_meta_box('cyb-meta-galeria', __('Galeria'), 'cyb_meta_box_galeria', 'page');
        add_meta_box('cyb-meta-box-slider-eventos', __('Slider portada'), 'cyb_meta_box_slider_portada', 'page');
    }

    if ($template_file == 'template-page/tpl-restaurante.php') {
        add_meta_box('cyb-meta-box-galeria-restaurante', __('Galeria'), 'cyb_meta_box_galeria_restaurante', 'page');
        add_meta_box('cyb-meta-box-slider-restaurante', __('Slider portada'), 'cyb_meta_box_slider_portada', 'page');
    }

    add_meta_box('cyb-meta-feregistro', __('Fecha de registro'), 'cyb_meta_feregistro', 'suscriptores');
    add_meta_box('cyb-meta-confirmfecha', __('Fecha de confirmacion'), 'cyb_meta_confirmfecha', 'suscriptores');
    add_meta_box('cyb-meta-status', __('Estatus'), 'cyb_meta_status', 'suscriptores');
    add_meta_box('cyb-meta-domain', __('Dominio'), 'cyb_meta_domain', 'suscriptores');
}

function cyb_meta_box_slider_portada(){
    global $post;
    $html='';
    $images_sliders = get_post_meta($post->ID,'wp_slider_portada',true);
    echo '<div class="items_slider">';
    if(!empty($images_sliders)){

        $contador = 0;
        foreach(json_decode($images_sliders) as $img){
            echo "<div class='gal-img' data-position='".$contador."'><img src='".$img->thumbnail."' ><a class='remove_img' name='".$img->name."'>Borrar</a></div>";
            $contador++;
        }

    }
    echo '<span></span></div>';
    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    $html .='<div class="img_gal">';
    $html .= "<input style='display:none;' id='image_location_slider' type='text' name='wp_slider_portada' value='".$images_sliders."' size='50'>";
    $html .= '<input data-contador="'.$contador.'" data-nameseccion="slider" type="button" class="onetarek-upload-button button" id="wp_custom_attachment" name="wp_custom_attachment" value="Agregar imagenes" />';
    $html .= '</div>';
    echo $html;
}


function cyb_meta_historia(){
    global $post;
    $historia = get_post_meta($post->ID,'historia',true);
    if(!empty($historia)){
        $historia_prod=$historia;
    }
    else{
        $historia_prod='';
    }

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    $html = '<div>';
    $html .= '<textarea style="width: 100%; height: 150px;" name="historia">'.$historia_prod.'</textarea>';
    $html .= '</div>';
    echo $html;
}

function cyb_meta_feregistro(){
    global $post;
    $created = get_post_meta($post->ID,'created',true);
    if(!empty($created)){
        $created_prod=$created;
    }
    else{
        $created_prod='';
    }

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    $html = '<div>';
    $html .= '<input style="width: 100%; type="text" name="created" value="'. date('d/m/Y', $created_prod).'">';
    $html .= '</div>';
    echo $html;
}

function cyb_meta_confirmfecha(){
    global $post;
    $confirmedat = get_post_meta($post->ID,'confirmed_at',true);
    if(!empty($confirmedat)){
        $confirmedat_prod=$confirmedat;
    }
    else{
        $confirmedat_prod='Suscripción no confirmada';
    }

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    $html = '<div>';
    $html .= '<input style="width: 100%; type="text" name="confirmed_at" value="'.$confirmedat_prod.'">';
    $html .= '</div>';
    echo $html;
}

function cyb_meta_status(){
    global $post;
    $status = get_post_meta($post->ID,'status',true);
    if(!empty($status)){
        $status_prod=$status;
    }
    else{
        $status_prod='';
    }

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    switch ($status_prod){

        case 0 :
            $html = '<div>';
            $html .= '<select style="width: 100%;">';
            $html .= '<option selected="selected" value="0">Suscripción no confirmada</option>';
            $html .= '<option value="1">Suscripción confirmada</option>';
            $html .= '</select>';
            $html .= '</div>';
            break;

        case 1:
            $html = '<div>';
            $html .= '<select style="width: 100%;" >';
            $html .= '<option value="0">Suscripción no confirmada</option>';
            $html .= '<option selected="selected" value="1">Suscripción confirmada</option>';
            $html .= '</select>';
            $html .= '</div>';

            break;

    }
    echo $html;
}

function cyb_meta_domain(){
    global $post;
    $domain = get_post_meta($post->ID,'domain',true);
    if(!empty($domain)){
        $domain_prod=$domain;
    }
    else{
        $domain_prod='';
    }

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    $html = '<div>';
    $html .= '<input style="width: 100%; type="text" name="domain" value="'.$domain_prod.'">';
    $html .= '</div>';
    echo $html;
}

function cyb_meta_box_galeria_restaurante() {
    global $post;
    $html='';
    $images = get_post_meta($post->ID,'wp_fancy_overlay_settings',true);
    echo '<div class="items_galeria">';
    if(!empty($images)){

        $contador = 0;
        foreach(json_decode($images) as $img){
            echo "<div class='gal-img' data-position='".$contador."'><img src='".$img->thumbnail."' ><a class='remove_img' name='".$img->name."'>Borrar</a></div>";
            $contador++;
        }

    }
    echo '<span></span></div>';
    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    $html .='<div class="img_gal">';
    $html .= "<input style='display:none;' id='image_location_galeria' type='text' name='wp_fancy_overlay_settings' value='".$images."' size='50'>";
    $html .= '<input data-contador="'.$contador.'" data-nameseccion="galeria" type="button" class="onetarek-upload-button button" id="wp_custom_attachment" name="wp_custom_attachment" value="Agregar imagenes" />';
    $html .= '</div>';
    echo $html;
}


function cyb_meta_box_galeria() {
    global $post;
    $html='';
    $images = get_post_meta($post->ID,'wp_fancy_overlay_settings',true);
    echo '<div class="items_galeria">';
    if(!empty($images)){

        $contador = 0;
        foreach(json_decode($images) as $img){
            echo "<div class='gal-img' data-position='".$contador."'><img src='".$img->thumbnail."' ><a class='remove_img' name='".$img->name."'>Borrar</a></div>";
            $contador++;
        }

    }
    echo '<span></span></div>';
    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    $html .='<div class="img_gal">';
    $html .= "<input style='display:none;' id='image_location_galeria' type='text' name='wp_fancy_overlay_settings' value='".$images."' size='50'>";
    $html .= '<input data-contador="'.$contador.'" data-nameseccion="galeria" type="button" class="onetarek-upload-button button" id="wp_custom_attachment" name="wp_custom_attachment" value="Agregar imagenes" />';
    $html .= '</div>';
    echo $html;
}

function cyb_meta_restaurante(){
    global $post;
    $restaurante = get_post_meta($post->ID,'restaurante',true);
    if(!empty($restaurante)){
        $restaurante_prod=$restaurante;
    }
    else{
        $restaurante_prod='';
    }

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    $html = '<div>';
    $html .= '<textarea style="width: 100%; height: 150px;" name="restaurante">'.$restaurante_prod.'</textarea>';
    $html .= '</div>';
    echo $html;
}

function cyb_meta_organizaevento(){
    global $post;
    $organizatuevento = get_post_meta($post->ID,'organizatuevento',true);
    if(!empty($organizatuevento)){
        $organizatuevento_prod=$organizatuevento;
    }
    else{
        $organizatuevento_prod='';
    }

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    $html = '<div>';
    $html .= '<textarea style="width: 100%; height: 150px;" name="organizatuevento">'.$organizatuevento_prod.'</textarea>';
    $html .= '</div>';
    echo $html;
}

function cyb_meta_hospedaje(){
    global $post;
    $hospedaje = get_post_meta($post->ID,'hospedaje',true);
    if(!empty($hospedaje)){
        $hospedaje_prod=$hospedaje;
    }
    else{
        $hospedaje_prod='';
    }

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    $html = '<div>';
    $html .= '<textarea style="width: 100%; height: 150px;" name="hospedaje">'.$hospedaje_prod.'</textarea>';
    $html .= '</div>';
    echo $html;
}

add_action( 'save_post', 'dynamic_save_postdata' );
function dynamic_save_postdata( $post_id ) {

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;
    if ( !isset( $_POST['dynamicMeta_noncename2'] ) )
        return;
    if ( !wp_verify_nonce( $_POST['dynamicMeta_noncename2'], plugin_basename( __FILE__ ) ) )
        return;


    if( isset( $_POST['wp_fancy_overlay_settings'] ) ){
        update_post_meta($post_id,'wp_fancy_overlay_settings',$_POST['wp_fancy_overlay_settings']);
    }


    if( isset( $_POST['wp_slider_portada'] ) ){
        update_post_meta($post_id,'wp_slider_portada',$_POST['wp_slider_portada']);
    }

    $historia = $_POST['historia'];
    update_post_meta($post_id,'historia',$historia);

    $restaurante = $_POST['restaurante'];
    update_post_meta($post_id,'restaurante',$restaurante);

    $organizatuevento = $_POST['organizatuevento'];
    update_post_meta($post_id,'organizatuevento',$organizatuevento);

    $hospedaje = $_POST['hospedaje'];
    update_post_meta($post_id,'hospedaje',$hospedaje);

}