<?php
add_action( 'init', 'crea_taxonimies' );
function crea_taxonimies(){
    register_taxonomy(
        'categoriasplatillos',
        'menu',
        array(
            'label' => __('Categoria platillos'),
            'sort' => true,
            'args' => array('orderby' => 'term_order'),
            'rewrite' => array('slug' => 'categoria_platillos'),
            'hierarchical'=>true,
        )
    );
}