<?php
add_action( 'init', 'create_post_type' );
function create_post_type()
{
    register_post_type('Menu',
        array(
            'labels' => array(
                'name' => __('Menu'),
                'singular_name' => __('menu')
            ),
            'public' => true,
            'has_archive' => true,
            'taxonomies' => array(),
            'supports' => array('title','editor','thumbnail')
        )
    );

    register_post_type('Suscriptores',
        array(
            'labels' => array(
                'name' => __('Suscriptores'),
                'singular_name' => __('suscriptores')
            ),
            'public' => true,
            'has_archive' => true,
            'taxonomies' => array(),
            'supports' => array('title')
        )
    );

    register_post_type('Eventos',
        array(
            'labels' => array(
                'name' => __('Eventos'),
                'singular_name' => __('evento')
            ),
            'public' => true,
            'has_archive' => true,
            'taxonomies' => array(),
            'supports' => array('title','editor','thumbnail')
        )
    );

    register_post_type('Salones',
        array(
            'labels' => array(
                'name' => __('Salones'),
                'singular_name' => __('salon')
            ),
            'public' => true,
            'has_archive' => true,
            'taxonomies' => array(),
            'supports' => array('title','editor','thumbnail')
        )
    );

    register_post_type('Alcobas',
        array(
            'labels' => array(
                'name' => __('Alcobas'),
                'singular_name' => __('alcoba')
            ),
            'public' => true,
            'has_archive' => true,
            'taxonomies' => array(),
            'supports' => array('title','editor','thumbnail')
        )
    );
}