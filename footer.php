<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package hacienda_teya
 */

?>

	</div><!-- #content -->
    <div class="overlaytotal"></div>
    <div class="modal_img text-center"><input class="posicion" type="hidden" name="posicion" value=""><a class="closemodalimgplatillo">X</a><img src=""><a class="next">next</a><a class="prev">prev</a></div>
	<footer id="colophon" class="large-12 columns site-footer" role="contentinfo">
		<div class="small-12 medium-6 large-6 columns copy">
            <p> &copy; <?php echo date('Y'); ?> Hacienda Teya. Todos los Derechos Reservados.</p>
		</div>
        <div class="small-12 medium-6 large-6 columns redes">
            <ul>
                <li><a target="_blank" href="https://www.facebook.com/LaHaciendaTeya/?fref=ts"><div class="glyph-icon flaticon-facebook3"></div></a></li>
                <li><a target="_blank" href="https://www.instagram.com/haciendateya/"><div class="glyph-icon flaticon-instagram19"></div></a></li>
            </ul>
        </div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
