<?php
/*
* Template Name: Confirmar suscripcion
*/
get_header();

if(isset($_GET['user'])) {
    update_post_meta($_GET['user'], 'confirmed_at', date('U'));
    update_post_meta($_GET['user'], 'status', 1);
}
?>
<div class="confirm_suscripcion">
    <h1 class="titulo_confirmar">Confirmar suscripción</h1>
    <p class="text-center">Tu suscripción a quedado confirmada</p>
</div>