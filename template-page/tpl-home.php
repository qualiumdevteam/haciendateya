<?php
/*
* Template Name: Home
*/
get_header();

?>
<div class="home">
    <section class="portada">
        <div class="overlay"></div>
        <div class="center">
            <h2>Un lugar mágico</h2>
            <h2>en yucatán</h2>
            <div class="contenido">
            <?php while ( have_posts() ) : the_post(); ?>
                <?php the_content(); ?>
            <?php endwhile ?>
            </div>
        </div>
        <a href="#restaurantes"><img class="arrow-down" src="<?php echo get_template_directory_uri() ?>/img/down.png"></a>
    </section>
    <section class="historia">
        <div class="center">
            <div class="contenido_historia">
            <h2>Fundada en el año 1683</h2>
            <div class="contenido">
                <p><?php echo nl2br(get_post_meta(get_the_ID(),'historia',true)); ?></p>
            </div>
            </div>
        </div>
    </section>
    <section id="restaurantes" class="restaurante">
        <div class="overlay"></div>
        <div class="small-12 medium-12 large-6 columns end contenido">
            <h1 class="titulo">Restaurante</h1>
            <p><?php echo nl2br(get_post_meta(get_the_ID(),'restaurante',true)); ?></p>
            <a class="text-color" href="<?php echo site_url(); ?>/restaurante/"><div class="btn_reservar">Contacto<img class="arrow-right" src="<?php echo get_template_directory_uri() ?>/img/right.png"></div></a>
            <a class="phone" href="tel:+019999880800">Reserva al teléfono: (01 999) 9 88 08 00</a>
        </div>
        <a href="#restaurantes"><img class="arrow-down" src="<?php echo get_template_directory_uri() ?>/img/down.png"></a>
    </section>
    <section class="organiza">
        <div class="overlay"></div>
        <div class="small-12 medium-12 large-6 columns end contenido">
            <h1 class="titulo">Cásate conmigo</h1>
            <p><?php echo get_post_meta(get_the_ID(),'organizatuevento',true); ?></p>
            <a class="text-color" href="<?php echo site_url(); ?>/organiza-tu-evento/"><div class="btn_reservar">Contacto<img class="arrow-right" src="<?php echo get_template_directory_uri() ?>/img/right.png"></div></a>
            <a class="phone" href="tel:+019999880800">Reserva al teléfono: (01 999) 9 88 08 00</a>
        </div>
        <a href="#restaurantes"><img class="arrow-down" src="<?php echo get_template_directory_uri() ?>/img/down.png"></a>
    </section>
    <section id="contacto" class="contacto">
        <div class="overlay"></div>
        <div class="center">
            <h1 class="titulo">Contacto</h1>
            <div class="form_contacto">
                <?php echo do_shortcode('[contact-form-7 id="9" title="Formulario de contacto 1"]') ?>
            </div>
        </div>
    </section>
</div>
<?php get_footer(); ?>