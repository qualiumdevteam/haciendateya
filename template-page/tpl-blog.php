<?php
/*
* Template Name: Blog
*/
get_header();

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
    'post_type' => 'post',
    'posts_per_page' => 3,
    'paged' => $paged,
    'order' => 'DESC'
);
$query = new WP_Query($args);
?>
<div class="blog">
    <h1 class="titulo_blog">Blog</h1>
    <div class="divisor"></div>
    <div class="articulos">
    <?php
    if($query->have_posts()){
    while($query->have_posts()) : $query->the_post();
    $feat_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
    ?>
        <a href="<?php echo get_the_permalink(); ?>">
        <div class="small-12 medium-6 large-4 columns item_blog">
            <div class="img_articulo text-center" style="background-image: url('<?php echo $feat_image; ?>');"></div>
            <h4 class="titulo_articulo text-center"><?php echo get_the_title(); ?></h4>
            <p><?php echo strip_tags(substr(get_the_content(),0,150)); ?>...</p>
        </div>
        </a>
    <?php  endwhile; }  ?>
    </div>
    <div class="paginav text-center"><span class="info_page">Página <?php echo $paged ?> de <?php echo $query->max_num_pages; ?> </span> <div class="number_page"><?php get_pagination($query); ?></div></div>
</div>
<div class="suscribete">
    <div class="form">
        <h1>Suscríbete</h1>
        <div class="form_suscribete">
            <form id="" method="post" class="">
                    <label>Email <span class="required">*</span></label>
                    <input type="text" name="" class="email validaremail" title="Email"  value="" />
                    <div class="text-center"><input class="wysija-submit addsuscriber" type="button" value="Suscribir" /></div>
                    <p class="mensaje_suscribers"></p>
            </form>
        </div>
    </div>
</div>
<?php get_footer(); ?>