<?php
/*
* Template Name: Evento
*/
get_header();
$args = array(
    'post_type' => 'eventos',
    'posts_per_page' => -1,
    'orderby'=> 'ID',
    'order' => 'asc',
);

$args2 = array(
    'post_type' => 'salones',
    'posts_per_page' => -1,
    'orderby'=> 'ID',
    'order' => 'asc',
);

$args3 = array(
    'post_type' => 'alcobas',
    'posts_per_page' => -1,
    'orderby'=> 'ID',
    'order' => 'asc',
);

$sliders= get_post_meta(get_the_ID(),'wp_slider_portada',true);
$convertido=json_decode($sliders);

?>
<div class="evento">
    <section class="portada">
        <?php foreach($convertido as $item_slider){ ?>
            <div class="slider_item" style="background-image: url('<?php echo $item_slider->full; ?>')"></div>
        <?php } ?>
        <div class="overlay"></div>
        <div class="contenido">
            <div class="small-12 medium-12 large-6 end columns descripcion_page">
                <h1 class="titulo">Cásate conmigo</h1>
                <?php while ( have_posts() ) : the_post(); ?>
                    <?php the_content(); ?>
                <?php endwhile ?>
                <div class="btn_reservar"><a href="#contacto">Contacto</a><img class="arrow-right" src="<?php echo get_template_directory_uri() ?>/img/right.png"></div>
                <a class="phone" href="tel:+019999482802">Reserva al teléfono: (01 999) 9 48 28 02</a>
            </div>
        </div>
        <a href="#tipo_evento"><img class="arrow-down" src="<?php echo get_template_directory_uri() ?>/img/down.png"></a>
    </section>
    <section id="tipo_evento" class="tipo_eventos">
        <div class="center">
            <h1 class="titulo">Tipos de eventos</h1>
            <?php $query = new WP_Query($args);
            while ( $query->have_posts() ) : $query->the_post(); ?>
                <?php $imgdestacada=wp_get_attachment_url( get_post_thumbnail_id(get_the_ID())); ?>
                <div class="small-12 medium-12 large-6 columns listado_eventos end">
                    <div class="small-12 medium-12 large-4 columns text-center">
                        <img src="<?php echo $imgdestacada; ?>">
                    </div>
                    <div class="small-12 medium-12 large-8 columns contenido_tipo">
                        <h4><?php echo get_the_title(); ?></h4>
                        <p><?php echo strip_tags(get_the_content()) ?></p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            <?php endwhile ?>
            <?php wp_reset_query(); ?>
        </div>
    </section>
    <section class="galeria">
        <h1 class="titulo">Galería</h1>
        <div class="small-12 medium-12 large-12 columns">
            <?php $galeria=get_post_meta(get_the_ID(),'wp_fancy_overlay_settings',true); ?>
            <?php $dat_galeria=json_decode($galeria); ?>
            <?php //print_r($dat_galeria); ?>
            <?php foreach($dat_galeria as $itemgal){ ?>
                <div class="small-12 medium-6 large-4 columns itemgal">
                    <img src="<?php echo $itemgal->full; ?>">
                </div>
            <?php } ?>
        </div>
    </section>
    <section id="salon" class="salones">
        <h1 class="titulo_seccion">Salones</h1>
        <div class="owl-carousel">
            <?php $query = new WP_Query($args2);
            while ( $query->have_posts() ) : $query->the_post(); ?>
            <?php $imgdestacada=wp_get_attachment_url( get_post_thumbnail_id(get_the_ID())); ?>
                <div style="background-image: url('<?php echo $imgdestacada ?>')" class="salon">
                    <div class="overlay"></div>
                    <div class="contenido_salon">
                        <h3><?php echo get_the_title(); ?></h3>
                        <p><?php echo strip_tags(get_the_content()) ?></p>
                    </div>
                </div>
            <?php endwhile ?>
            <?php wp_reset_query(); ?>
        </div>
    </section>
    <section class="habitaciones">
        <div id="hospedaje" class="texto_hospedaje">
            <h1 class="text-center">Hospedaje</h1>
            <p class="text-center"><?php echo nl2br(get_post_meta(get_the_ID(),'hospedaje',true)); ?></p>
        </div>
        <div id="alcobas" class="alcobas">
            <h1 class="titulo_seccion">Alcobas</h1>
            <div class="owl-carousel">
                <?php $query = new WP_Query($args3);
                while ( $query->have_posts() ) : $query->the_post(); ?>
                        <?php $imgdestacada=wp_get_attachment_url( get_post_thumbnail_id(get_the_ID())); ?>
                    <div style="background-image: url('<?php echo $imgdestacada ?>')" class="salon">
                        <div class="overlay"></div>
                        <div class="contenido_salon">
                            <h3><?php echo get_the_title(); ?></h3>
                            <p><?php echo strip_tags(get_the_content()) ?></p>
                        </div>
                    </div>
                <?php endwhile ?>
                <?php wp_reset_query(); ?>
            </div>
        </div>
    </section>
    <section id="contacto" class="contacto">
        <div class="overlay"></div>
        <div class="center">
            <h1 class="titulo">Reserva</h1>
            <div class="form_contacto">
                <?php echo do_shortcode('[contact-form-7 id="194" title="Formulario de contacto eventos"]') ?>
            </div>
        </div>
    </section>
</div>
<?php get_footer(); ?>