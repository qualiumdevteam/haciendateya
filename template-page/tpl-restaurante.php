<?php
/*
* Template Name: Restaurante
*/
get_header();
$args = array(
    'orderby'           => 'name',
    'order'             => 'ASC',
    'hide_empty'        => false,
    'exclude'           => array(),
    'exclude_tree'      => array(),
    'include'           => array(),
    'number'            => '',
    'fields'            => 'all',
    'slug'              => '',
    'parent'            => '',
    'hierarchical'      => true,
    'child_of'          => 0,
    'childless'         => false,
    'get'               => '',
    'name__like'        => '',
    'description__like' => '',
    'pad_counts'        => false,
    'offset'            => '',
    'search'            => '',
    'cache_domain'      => 'core'
);

$terms = get_terms('categoriasplatillos', $args);

$sliders= get_post_meta(get_the_ID(),'wp_slider_portada',true);
$convertido=json_decode($sliders);
//print_r($convertido[0]->full);

?>
<div class="restaurante_template">
    <section class="portada">
        <?php foreach($convertido as $item_slider){ ?>
            <div class="slider_item" style="background-image: url('<?php echo $item_slider->full; ?>')"></div>
        <?php } ?>
        <div class="overlay"></div>
        <div class="contenido">
            <div class="small-12 medium-12 large-6 end columns descripcion_page">
                <h1 class="titulo">Restaurante</h1>
                <?php while ( have_posts() ) : the_post(); ?>
                    <?php the_content(); ?>
                <?php endwhile ?>
                <div class="btn_reservar"><a href="#contacto">Contacto</a><img class="arrow-right" src="<?php echo get_template_directory_uri() ?>/img/right.png"></div>
                <a class="phone" href="tel:+019999880800">Reserva al teléfono: (01 999) 9 88 08 00</a>
            </div>
        </div>
        <a href="#menu"><img class="arrow-down" src="<?php echo get_template_directory_uri() ?>/img/down.png"></a>
    </section>
    <section id="menu" class="menu">
        <div class="row center">
            <h1 class="titulo">Menú</h1>
        <?php foreach ( $terms as $term ) { ?>
            <div class="small-12 medium-12 large-12 columns listado_platillo">
                <div class="small-12 medium-12 large-2 columns text-center img_platillo"><?php the_category_thumbnail($term->term_id) ?></div>
                <div class="small-12 medium-12 large-10 columns titulo_platillo">
                    <p data-id="<?php echo $term->term_id; ?>" class="name_categoria"><?php echo $term->name; ?><span class="more">+</span></p>
                    <div class="platillos" id="platillos_<?php echo $term->term_id; ?>">
                    </div>
                </div>
                <div id="spinner_<?php echo $term->term_id; ?>" class="spinner">
                    <div class="spinner-container container1">
                        <div class="circle1"></div>
                        <div class="circle2"></div>
                        <div class="circle3"></div>
                        <div class="circle4"></div>
                    </div>
                    <div class="spinner-container container2">
                        <div class="circle1"></div>
                        <div class="circle2"></div>
                        <div class="circle3"></div>
                        <div class="circle4"></div>
                    </div>
                    <div class="spinner-container container3">
                        <div class="circle1"></div>
                        <div class="circle2"></div>
                        <div class="circle3"></div>
                        <div class="circle4"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        <?php } ?>
        </div>
    </section>
    <section class="galeria">
        <h1 class="titulo">Galería</h1>
        <div class="small-12 medium-12 large-12 columns">
        <?php $galeria=get_post_meta(get_the_ID(),'wp_fancy_overlay_settings',true); ?>
        <?php $dat_galeria=json_decode($galeria); ?>
            <?php //print_r($dat_galeria); ?>
        <?php foreach($dat_galeria as $itemgal){ ?>
            <div class="small-12 medium-6 large-4 columns itemgal">
                <img src="<?php echo $itemgal->full; ?>">
            </div>
        <?php } ?>
        </div>
    </section>
    <section id="contacto" class="contacto">
        <div class="overlay"></div>
        <div class="center">
            <h1 class="titulo">Reserva</h1>
            <div class="form_contacto">
                <?php echo do_shortcode('[contact-form-7 id="71" title="Formulario de contacto restaurante"]') ?>
            </div>
        </div>
    </section>
</div>
<?php get_footer(); ?>