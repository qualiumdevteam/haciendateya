<?php
/*
* Template Name: Ubicación
*/
get_header();
?>

<script>

    function initialize() {

        var coordenadas = new google.maps.LatLng(20.936664, -89.521329);
        var coordenadas2 = new google.maps.LatLng(20.936664, -89.521329);

        var colores = [
            {
                featureType: "all",
                elementType: "all",
                stylers: [
                    {saturation: -100}
                ]
            }
        ];
        var opciones = {
            zoom: 16,
            center: coordenadas,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            panControl: false,
            zoomControl: false,
            scaleControl: false
        };

        var mapa = new google.maps.Map(document.getElementById('pony'), opciones);

        var estilo = new google.maps.StyledMapType(colores);
        mapa.mapTypes.set('mapa-bn', estilo);
        mapa.setMapTypeId('mapa-bn');

        var marcador = new google.maps.Marker({
            position: coordenadas2,
            map: mapa
        });

    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<div id="pony" class="ubicacion">
</div>
<section class="infocontacto">
    <div class="contenedor_info">
        <?php while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile ?>
    </div>
</section>