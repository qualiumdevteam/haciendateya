<?php
/**
 * hacienda teya functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package hacienda_teya
 */

if ( ! function_exists( 'hacienda_teya_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */

function maps_api() {
    wp_register_script( 'maps-google', 'http://maps.googleapis.com/maps/api/js?sensor=false');
}

add_action('init', 'maps_api');

function hacienda_teya_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on hacienda teya, use a find and replace
	 * to change 'hacienda-teya' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'hacienda-teya', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

    add_theme_support('category-thumbnails');

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'hacienda-teya' ),
	) );

    register_nav_menus( array(
        'movil' => esc_html__( 'Movil Menu', 'hacienda-teya' ),
    ) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'hacienda_teya_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // hacienda_teya_setup
add_action( 'after_setup_theme', 'hacienda_teya_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function hacienda_teya_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'hacienda_teya_content_width', 640 );
}
add_action( 'after_setup_theme', 'hacienda_teya_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function hacienda_teya_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'hacienda-teya' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'hacienda_teya_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function hacienda_teya_scripts() {
	wp_enqueue_style( 'hacienda-teya-style', get_stylesheet_uri() );
    wp_enqueue_style( 'foundation-min', get_template_directory_uri() . '/css/foundation.min.css', array(), '20120206', false );
    wp_enqueue_style( 'carosel', get_template_directory_uri() . '/css/owl.carousel.css', array(), '20120206', false );
    wp_enqueue_style( 'theme-carosel', get_template_directory_uri() . '/css/owl.theme.default.css', array(), '20120206', false );
    wp_enqueue_style( 'icons', get_template_directory_uri() . '/fonts/icons/flaticon.css', array(), '20120206', false );
    wp_enqueue_style( 'spinnner', get_template_directory_uri() . '/css/spinner.css', array(), '20120206', false );

    wp_enqueue_script( 'hacienda-teya-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
	wp_enqueue_script( 'hacienda-teya-owlcarousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array(), '20120206', true );
    wp_enqueue_script( 'hacienda-teya-main', get_template_directory_uri() . '/js/main.js', array('jquery'), '20120206', true );
	wp_enqueue_script( 'hacienda-teya-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
    wp_enqueue_script( 'maps-google' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'hacienda_teya_scripts' );

add_action('admin_print_scripts', 'wp_fancy_commerce_admin_scripts');
function wp_fancy_commerce_admin_scripts(){
    wp_enqueue_script( 'hacienda-mainadmin', get_template_directory_uri() . '/js/main_admin.js', array(), '20120206', true );
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    wp_enqueue_script('common');
    wp_enqueue_script('jquery');
    wp_enqueue_style( 'adminstyle', get_template_directory_uri() . '/css/admin_style.css', array(), '20120206', false );
}

require get_template_directory() . '/inc/metaboxes.php';

require get_template_directory() . '/inc/post-types.php';

require get_template_directory() . '/inc/taxonomies.php';

add_action('wp_ajax_nopriv_get_platillos', 'get_platillos');
add_action('wp_ajax_get_platillos', 'get_platillos');
function get_platillos(){
    $args = array(
        'post_type' => 'menu',
        'posts_per_page' => -1,
        'orderby'=> 'ID',
        'order' => 'asc',
        'tax_query' => array(
            array(
                'taxonomy' => 'categoriasplatillos',
                'terms' => $_POST['idcategory']
            )
        )
    );
    $platillos=array();
    $query = new WP_Query($args);
    if($query->have_posts()):
        while ($query->have_posts()) : $query->the_post();
            $imgdestacada=wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
            $datos_platillos=array('nombre'=>get_the_title(),'imgplatillo'=>$imgdestacada);
            array_push($platillos,$datos_platillos);
        endwhile;
        echo json_encode($platillos);
    endif;
    exit();
}

add_action('wp_ajax_nopriv_addsuscribers', 'addsuscribers');
add_action('wp_ajax_addsuscribers', 'addsuscribers');
function addsuscribers(){
    global $wpdb;

    if(isset($_POST['email'])){ $email=  $_POST['email']; } else { $email=''; }
    $args = array(
        'post_type' => 'suscriptores',
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key' => 'email',
                'value' => "$email",
            ),
        )
    );
    $the_query  = new WP_Query( $args );

    if($the_query->have_posts()){
        $result=0;
    }else{
        $dominio= explode('@',$email);

        $my_post = array();
        $my_post['post_title'] = $email;
        $my_post['post_type'] = 'suscriptores';
        $my_post['post_status'] = 'publish';
        $my_post['post_author'] = 1;
        $my_post['post_category'] = array(0);

        $post_id = wp_insert_post($my_post);

        update_post_meta($post_id,'email',$email);
        update_post_meta($post_id,'confirmed_at','');
        update_post_meta($post_id,'created',date('U'));
        update_post_meta($post_id,'status',0);
        update_post_meta($post_id,'domain',$dominio[1]);

        //notifi_pagos($email,$post_id);
        $result=$post_id;
    }

    echo $result;

    exit();

    // wp_suscribers_list
}

function notifi_pagos($email2,$post_id){

    add_filter( 'wp_mail_from', 'my_mail_from' );
    function my_mail_from( $email )
    {
        return "noreply@eternitta.com.mx";
    }

    add_filter( 'wp_mail_from_name', 'my_mail_from_name' );
    function my_mail_from_name( $name )
    {
        return "Eternitta";
    }

    $to=$email2;
    $subject2='Confirmacion de suscripción';
    $headers2 = 'Reply-to: noreply@eternitta.com.mx' . "\r\n";
    $message2 ='<div style="text-align: center; background: rgba(0, 0, 0, 0.7);; padding: 5px;"><img style="width: 160px;" src="http://haciendateya.com/new/wp-content/themes/hacienda-teya/img/logo.png"></div><br/>';
    $message2.='<p style="text-align: center;">Te has suscrito a nuestro boletin haz click en el boton para confirmar tu suscripción</p>';
    $message2.='<a href="'.site_url().'/confirmar-suscripcion/?user='.$post_id.'"><p style="text-align: center; padding: 5px; border: 2px solid #BF2E1B;">Confirmar</p></a></div>';
    $message2.='<div style="text-align: center; background: #E6E6E6; padding: 5px; color: #00ACB4;">';
    $message2.='<p style="margin-bottom: 0px;">No responda este correo electrónico, ya que no estamos controlando esta bandeja de entrada. Para comunicarse con nosotros, haga clic <a style="color: #fff;" href="http://haciendateya.com/new/#contacto">aquí</a>.</p>
            <p style="margin-bottom: 0px;">
                Copyright &copy; 20015-'.date('Y').' Hacienda Teya. Todos los derechos reservados.
            </p></div>';
    add_filter('wp_mail_content_type',create_function('', 'return "text/html";'));

    wp_mail( $to, $subject2, $message2, $headers2);
}

function get_pagination($query){
    $big = 999999999; // need an unlikely integer


    echo paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        //'base' => @add_query_arg('page','%#%'),
        'format' => '?paged=%#%',
        'prev_text'    => __('<'),
        'current' => max(1,get_query_var('paged') ),
        'next_text'    => __('>'),
        'total' => $query->max_num_pages,
    ) );
}

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
