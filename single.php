<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package hacienda_teya
 */

get_header();

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
    'post_type' => 'post',
    'posts_per_page' => 3,
    'paged' => $paged,
    'order' => 'DESC'
);
$query = new WP_Query($args);
?>

	<div id="primary" class="detalle_articulo content-area">

		<?php while ( have_posts() ) : the_post(); ?>

            <?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) ); ?>
            <div style="background-image: url('<?php echo $feat_image ?>')" class="img_detalle_articulo"><div class="overlay"></div></div>
			<?php get_template_part( 'template-parts/content', 'single' ); ?>

			<?php //the_post_navigation(); ?>

			<?php
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					//comments_template();
				endif;
			?>

		<?php endwhile; // End of the loop. ?>
        <?php wp_reset_query() ?>

        <h3 class="titulo_relacionado text-center">Relacionados</h3>
        <div class="divisor"></div>
        <div class="relacionados">
        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
            <?php $feat_image2 = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) ); ?>
            <div class="item_relacionado">
                <div style="background-image: url('<?php echo $feat_image2 ?>')" class="img_relacionado"><div class="overlay"></div></div>
                <h5 class="titulo_itemrelacionado"><?php echo get_the_title(); ?></h5>
                <p><?php echo strip_tags(substr(get_the_content(),0,150)); ?>...</p>
            </div>
        <?php endwhile; ?>
        </div>

	</div><!-- #primary -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
