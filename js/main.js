jQuery(document).ready(function(){

    if(window.location.hash) {
        var x = location.hash;
        var target = jQuery(x);
        if (target.length) {
            position=target.offset().top;
            jQuery('html,body').animate({
                scrollTop: position
            }, 1000);
        }
    } else {

    }

    if(jQuery('body').hasClass('page-id-9') || jQuery('body').hasClass('page-id-11')){
        var sliders=jQuery('.slider_item');
        var contador=-1;
        imagenes();
        function imagenes(){
            //jQuery(sliders[contador]).removeClass('slider_item')
            if(contador==sliders.length-1){
                contador=0;
            }else{
                contador++;
            }
            jQuery('.slider_item').removeClass('slider_item_show');
            jQuery(sliders[contador]).addClass('slider_item_show');

            //jQuery(sliders[contador]).addClass('slider_item_show');
        }
        iniciarintervalo ();
        function iniciarintervalo(){
            setInterval(function(){
                imagenes();
            }, 4000);
        }
        //var objetos_sliders=JSON.parse(sliders);

    }

    if(jQuery('body').hasClass('page-id-157')){
        jQuery('#menu-item-19').click(function(){
            alert('hola');
            window.location=location.origin+'/new/#contacto';
        })
    }

    jQuery('.close_menu').click(function(){
        jQuery('.main-navigation_movil').css({
            'opacity': '0',
            'visibility': 'hidden'
        });
    })

    jQuery('.barra_menu').on('click','img',function(){
        jQuery('.main-navigation_movil').css({
            'opacity': '1',
            'visibility': 'visible'
        });
    });

    jQuery('#movil-menu').on('click','li',function(){
        if (jQuery(this).find('ul').is(':hidden')) {

            jQuery(this).find('ul').slideDown();
        }
        else {
            jQuery(this).find('ul').slideUp();
        }
    })

    jQuery('.name_categoria').click(function(){

        var idcategory=jQuery(this).data('id');
        var item_platillo='';

        if(jQuery(this).parent().hasClass('activo')){

            jQuery(this).parent().removeClass('activo');
            jQuery(this).parent().find('.platillos').empty();
            jQuery(this).find('.more').text('+');

        }else {
                jQuery(this).parent().addClass('activo');
                jQuery(this).find('.more').text('-');
                jQuery.ajax({
                    url: '/new/wp-admin/admin-ajax.php',
                    timeout: 8000,
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        action: 'get_platillos',
                        idcategory: idcategory
                    },
                    beforeSend: function () {
                        jQuery('#spinner_' + idcategory).show();
                    },
                    success: function (data) {
                        jQuery('#spinner_' + idcategory).hide();
                        if(data) {
                            for (var e = 0; e < data.length; e++) {
                                item_platillo += '<li data-posicion="'+e+'"><img data-img="' + data[e].imgplatillo + '" class="platillo_img" src="' + data[e].imgplatillo + '"><span>' + data[e].nombre + '</span></li>'
                            }
                            jQuery('#platillos_' + idcategory).html('<ul>' + item_platillo + '</ul>');
                        }else{
                            jQuery('#platillos_' + idcategory).html('<p class="vacio">No se encontraron platillos</p>');
                        }
                    },
                    error: function (err) {
                        jQuery('#spinner_' + idcategory).hide();
                    }
                })
        }
    })

    jQuery('.platillos').on('click','li',function(){

        var img_data=jQuery(this).find('img').data('img');
        var posicion=jQuery(this).data('posicion');
        jQuery('.overlaytotal').fadeIn();
        jQuery('.modal_img').show();
        jQuery('.modal_img').find('img').attr('src',img_data);
        jQuery('.posicion').val(posicion);
    })

    jQuery('.closemodalimgplatillo').click(function(){
        jQuery('.overlaytotal').fadeOut();
        jQuery(this).parent().hide();
    });

    jQuery('.next').click(function(){

    })

    jQuery('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:false,
        URLhashListener:false,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    })


    jQuery('.relacionados').owlCarousel({
        loop:false,
        margin:10,
        nav:false,
        dots: false,
        URLhashListener:false,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:2
            }
        }
    })

    jQuery('.itemgal').click(function(){
        var url_img=jQuery(this).find('img').attr('src');
        jQuery('.overlaytotal').fadeIn();
        jQuery('.modal_img').show();
        jQuery('.modal_img').find('img').attr('src',url_img);
    })

    jQuery('.addsuscriber').click(function(){
        var email = jQuery('.email').val();
        if(email!='' && validemail(email)){
            jQuery.ajax({
                url: '/new/wp-admin/admin-ajax.php',
                timeout: 8000,
                type: 'POST',
                data: {
                    action: 'addsuscribers',
                    email: email
                },
                beforeSend: function () {

                },
                success: function (data) {
                    if(data!=0){
                        jQuery('.mensaje_suscribers').text('Grascias por suscribirse');
                    }else{
                        jQuery('.mensaje_suscribers').text('Este correo ya se encuentra suscrito');
                    }
                },
                error: function (err) {

                }
            })
        }else{
            if(!validemail(email) || email==''){
                jQuery('.email').css('border','2px solid #BF2E1B')
            }
        }
    })

    jQuery('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = jQuery(this.hash);
            target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                position=target.offset().top-35;
                jQuery('html,body').animate({
                    scrollTop: position
                }, 1000);
                return false;
            }
        }
    });

})

function validemail(email){
    var expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if ( expr.test(email) ){
        return true;
    }else{
        return false;
    }
}